package task_2;

@FunctionalInterface
public interface Printable {
        void print();

}
