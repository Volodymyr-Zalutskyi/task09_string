package task_2;

import java.util.*;

public class Application {
    private Map<String, String> menu;
    private Map<String, Printable> interfaceMenu;

    private static Scanner input = new Scanner(System.in);

    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("Q", bundle.getString("Q"));
    }

    public Application() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        interfaceMenu = new LinkedHashMap<>();
        interfaceMenu.put("1", this::testStringUtils);
        interfaceMenu.put("2", this::internationalMenuEnglish);
        interfaceMenu.put("3", this::nationalMenuUkrainian);
        interfaceMenu.put("4", this::MenuSpanish);
        interfaceMenu.put("5", this::MenuTurkish);
    }

    private void internationalMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }


    private void testStringUtils() {
        setMenu();
        show();
    }

    private void nationalMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void MenuSpanish() {
        locale = new Locale("es");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    private void MenuTurkish() {
        locale = new Locale("tr");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    //---------------------------------------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                interfaceMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }


}
