package task_1;

public class StringUtils {

    public static String concatenates (String...param) {
        int i = param.length;
        if (i == 0) {
            return null;
        }
        String newString = "";
        for (String concat: param) {
            newString += concat+" ";
        }
        return newString;
    }
}
