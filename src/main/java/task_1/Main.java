package task_1;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println(StringUtils.concatenates("concatenates", "all", "parameters", "and", "return", "String"));


        String text = "Hello, my name is John. What's your name?";
        text = text.replace(",", " , "); // Add an space before and after the comma
        text = text.replace(".", " '.' "); // Add an space before and after the point
        text = text.replace("  ", " "); // Remove possible double spaces
        String[] mListtext = text.split(" "); //Generates your list
        System.out.println(Arrays.toString(mListtext));

        String[] mList = StringUtils.concatenates("concatenates", "all", "parameters", "and", "return", "String")
                .replace(" ", ",")
                .split(" ");

        System.out.println("************************************************");
        System.out.println(Arrays.toString(mList));
        System.out.println("************************************************");

        List<String> stringList = Pattern.compile("[you]")
                .splitAsStream("theyou")
                .collect(Collectors.toList());

        stringList.forEach(s -> System.out.println(s));

        System.out.println("------------------------------------------------");
        // Create a pattern to match breaks
        Pattern p = Pattern.compile("[,\\s]+");
        // Split input with the pattern
        String[] result =
                p.split("one,two, three   four ,  five");
        for (int i = 0; i < result.length; i++)
            System.out.println(result[i]);



        String pattern = "[aeiou]";
        Pattern pat = Pattern.compile(pattern);
        Matcher m = pat.matcher(text);


        while (m.find()) {
            System.out.println(text.substring(m.start(), m.end()) + " - replaced ");
        }

        System.out.println("");

        String replace = "_";
        text = m.replaceAll(replace);
        System.out.println(text);
    }

}